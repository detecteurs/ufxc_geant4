# X-ray matter simulatons



## Getting started

Geant4 simulation project developed at SOLEIL to study the interaction of photons in a given material

## How to use it

- It's highly recommended to use it in the SUMO cluster of SOLEIL 
- If you want to use it in your local computer you first need to install the geant4 shoftware
- In SUMO you just need to connect to the cluster and then charge the required modules: 

```
# Modules:
module load physics/geant4/4.11
module load tools/gcc/9.2.0

# Environment variables for Geant4 and ROOT.
export GEANT4=/usr/local/applications/physics/geant4/4.11/
export ROOT6=/usr/local/applications/tools/root6/
export BOOST=/usr/local/applications/tools/boost/1.78

# Geant4 and ROOT environment variables are loaded.
source $GEANT4/bin/geant4.sh
source $ROOT6/bin/thisroot.sh


# ROOT library.
export LD_LIBRARY_PATH=$BOOST/lib:$LD_LIBRARY_PATH:$ROOT6/lib:$GEANT4/lib64:/usr/lib64/:/usr/lib

```

You can just add the previous commnads in a setup_geant4.sh file and just use 
```
source setup_geant4.sh

```
when you start a new session in SUMO 

## Installing the sofwtware 

If is the first time that you are using it, you can just clone it by doing: 

```
git clone git@gitlab.synchrotron-soleil.fr:detecteurs/ufxc_geant4.git

```
The go to the ufxc_geant4 folder 

```
cd ufxc_geant4
mkdir build 
cd build
cmake ../
make 

```
That should install the software. You can just run in the terminal: 
```
./G4SOS 
```
That will show you the current geomtry. To run an event just do: 
```
/run/beamOn 1
```
Then you can use a macro in batch mode (for example test.mac) if you are running thousands of events. To change the settings as energy of photons, degree of polarization or angle of polarization, etc just modify the test.mac file  

```
#just in case, it shoud be true by default
/process/em/fluo true
/process/em/auger true
/process/em/augerCascade true
/process/em/pixe true

#Uncomment this line to set the output directory, by default you will save the file in the same build directory
#/G4SOS/det/setOutputDirectory /nfs/tegile/work/experiences/detecteurs/manzanillas/LEAPS_INNOV/Geant4_output/
/G4SOS/det/setSetupName my_baby_ufxc
#select output format, options are: csv root hdf5
/G4SOS/det/setDataType csv
#Choise of source type: 0 gamma (xrays), 1 Fe-55,2 Cs-137; 1 Bi-207; 2 Sr-90; 3 e-
/run/initialize
/G4SOS/gun/sourceType 0
#the default is rectangular geometry of the beam but can be changed to circular uncomenting the next line
#/G4SOS/gun/sourceDiameter 0.1 mm
#These values should be adjunsted according to the information about the beam size 
/G4SOS/gun/sourceSizeX 0.1 mm 
/G4SOS/gun/sourceSizeY 0.03 mm 
/G4SOS/gun/sourceEnergy 7.0  keV
#For horizontal polarization set angle to 0, for vertical polarization set angle to  90, etc 
/G4SOS/gun/sourceGammaPolarizationAngle 90
/G4SOS/gun/sourceGammaPolarizationDegree 0.9

/run/beamOn 9000000


```

- Then you can run it from your build file 


```
./G4SOS -m test.mac

```

This will create a .csv file or other format depending on what was your choice where it's stored the hits information position of interaction, number of event, energy ...
You can use the code of your choice to analyze the data  

